////////////////////////////////////////////////////////////////////////////////
// This is a data object for the various routines to use as a data store.
// Each strategy for the routines will declare its own version of Appearance,
// Form and Animation. But they will all know how to access an Entity.
////////////////////////////////////////////////////////////////////////////////


class Entity
{
    Appearance appearance;
    Form form;
    Animation animation;

	Entity (Appearance app, Form fo, Animation anim)
	{
		appearance = app;
		form = fo;
		animation = anim;
	}

	void draw (float time)
	{
		animation.apply (form, time);
		appearance.draw (form);
	}

	boolean finished (float time)
	{
		return animation.finished (time);
	}
}
